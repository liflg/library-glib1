#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CONFIGUREOPTIONS="--enable-debug=yes"
    else
        CONFIGUREOPTIONS="--enable-debug=no"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo 'Optimization level '$OPTIMIZATION' is not yet implemented!'
        exit 1
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( cd source
      patch -Np1 -i ../patches/glib-1.2.10-gcc34-1.patch )

    ( cd "$BUILDDIR"
      ../source/configure \
         --prefix="$PREFIXDIR" \
         --disable-static \
         $CONFIGUREOPTIONS
      make install
      chmod -v 755 "$PREFIXDIR"/lib/{libgmodule-1.2.so.0.0.10,libgmodule.la}
      rm -rf "$PREFIXDIR"/{bin,info,lib/*.la,lib/pkgconfig,man,share})

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)

    # workaround for https://bitbucket.org/liflg/library-glib1/issue/1
    ( cd $PREFIXDIR/include
      ln -s ../lib/glib/include/glibconfig.h . )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="$PWD/build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-glib1.txt

rm -rf "$BUILDDIR"

echo "Glib1 for $MULTIARCHNAME is ready."
