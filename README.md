Website
=======
http://www.gtk.org/

License
=======
GNU LGPL version 2.1 (see the file source/COPYING)

Version
=======
1.2.10

Source
======
glib-1.2.10.tar.gz (sha256: 6e1ce7eedae713b11db82f11434d455d8a1379f783a79812cd2e05fc024a8d9f)

Required by
===========
* gtk1